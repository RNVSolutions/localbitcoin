<?php defined('BASEPATH') or exit('No direct script access allowed');

class AdminController extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        if(!_is_logged_in()) {
            redirect("/admin/login");
        }
    }
	
	//Dashboard page
	public function index()
	{
		$data['title'] = 'Log In';		
		$data['view'] = 'admin-end/index';
		$this->load->view('template-structure/admin-master-view',$data);
	}
	//data table page
	public function table()
	{
		$data['title'] = 'Table';		
		$data['view'] = 'admin-end/datatable-view';
		$this->load->view('template-structure/admin-master-view',$data);
	}

}
