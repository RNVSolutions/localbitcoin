<?php defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model("LoginModel");
    }
	// User
	//login page
	public function login()
	{
		$data['title'] = 'Login';
		$this->load->view('front-end/authorization-pages/login-view', $data);
	}

	//Register page
	public function register()
	{
		$data['title'] = 'Register';
		$this->load->view('front-end/authorization-pages/register-view', $data);
	}

	//Forgot password page
	public function forgotPassword()
	{
		$data['title'] = 'Password Recovery';
		$this->load->view('front-end/authorization-pages/forgot-password-view', $data);
	}

	//Admin

	//login page
	public function adminLogin()
	{
		$data['title'] = 'Login';
		// $this->session->userdata('user_data')
		if(isset($_SESSION['user_data'])) {
			redirect('/admin');
		} else {
			if(isset($_POST['saveLogin'])) {
				$userresult = $this->LoginModel->user_detail_by_username_or_email($_POST['username']);
				if($userresult) {
					if($userresult['status']=='1') {
						if(password_verify($_POST['password'],$userresult['password'])) {
							$this->LoginModel->update_login($userresult['id']);
							$userresult['user_data'] = 1;
							$this->session->set_userdata($userresult);
							redirect('/admin');
						} else {
							$data['message'] = "Invalid password";
						}
					} else {
						$data['message'] = "Your account is temporary blocked. Please contact admin.";
					}
				} else {
					$data['message'] = "Please enter valid details.";
				}
			}
			$this->load->view('admin-end/authorization-pages/login-view', $data);
		}
	}

	//Forgot password page
	public function adminForgotPassword()
	{
		$data['title'] = 'Password Recovery';
		if(isset($_SESSION['user_data'])) {
			redirect('/admin');
		} else {
			$this->load->view('admin-end/authorization-pages/forgot-password-view', $data);
		}
		
	}
	public function adminLogout() 
	{	
		$this->LoginModel->update_logout($_SESSION['id']);
		$this->session->unset_userdata('user_data');
		$this->session->sess_destroy();
		redirect('/admin/login');
	}
	
	

//echo password_hash($_POST['password'],PASSWORD_BCRYPT,['cost'=>9]);
}
