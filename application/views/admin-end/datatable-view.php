<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/public/admin-assets/plugins/table/datatable/datatables.css">
<link rel="stylesheet" type="text/css" href="/public/admin-assets/plugins/table/datatable/custom_dt_html5.css">
<link rel="stylesheet" type="text/css" href="/public/admin-assets/plugins/table/datatable/dt-global_style.css">
<!-- END PAGE LEVEL STYLES -->


<div class="layout-px-spacing">
	<div class="row layout-top-spacing">

		<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
			<div class="widget-content widget-content-area br-6">
				<div class="table-responsive mb-4 mt-4">
					<table id="html5-extension" style="width:100%">
						<thead>
							<tr>
								<th>Name</th>
								<th>Position</th>
								<th>Office</th>
								<th>Age</th>
								<th>Salary</th>
								<th class="text-center">Status</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Tiger Nixon</td>
								<td>System Architect</td>
								<td>Edinburgh</td>
								<td>61</td>
								<td>$320,800</td>
								<td>
									<div class="t-dot bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Normal"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Cedric Kelly</td>
								<td>Senior Javascript Developer</td>
								<td>Edinburgh</td>
								<td>22</td>
								<td>$433,060</td>
								<td>
									<div class="t-dot bg-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Low"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Sonya Frost</td>
								<td>Software Engineer</td>
								<td>Edinburgh</td>
								<td>23</td>
								<td>$103,600</td>
								<td>
									<div class="t-dot bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Medium"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Quinn Flynn</td>
								<td>Support Lead</td>
								<td>Edinburgh</td>
								<td>22</td>
								<td>$342,000</td>
								<td>
									<div class="t-dot bg-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="High"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Dai Rios</td>
								<td>Personnel Lead</td>
								<td>Edinburgh</td>
								<td>35</td>
								<td>$217,500</td>
								<td>
									<div class="t-dot bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Normal"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Gavin Joyce</td>
								<td>Developer</td>
								<td>Edinburgh</td>
								<td>42</td>
								<td>$92,575</td>
								<td>
									<div class="t-dot bg-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="High"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Martena Mccray</td>
								<td>Post-Sales support</td>
								<td>Edinburgh</td>
								<td>46</td>
								<td>$324,050</td>
								<td>
									<div class="t-dot bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Medium"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Jennifer Acosta</td>
								<td>Junior Javascript Developer</td>
								<td>Edinburgh</td>
								<td>43</td>
								<td>$75,650</td>
								<td>
									<div class="t-dot bg-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="High"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
							<tr>
								<td>Shad Decker</td>
								<td>Regional Director</td>
								<td>Edinburgh</td>
								<td>51</td>
								<td>$183,000</td>
								<td>
									<div class="t-dot bg-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Low"></div>
								</td>
								<td class="text-center"> <button class="btn btn-primary">view</button> </td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<th>Name</th>
								<th>Position</th>
								<th>Office</th>
								<th>Age</th>
								<th>Salary</th>
								<th class="text-center">Status</th>
								<th class="text-center">Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/public/admin-assets/plugins/table/datatable/datatables.js"></script>
<!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
<script src="/public/admin-assets/plugins/table/datatable/button-ext/dataTables.buttons.min.js"></script>
<script src="/public/admin-assets/plugins/table/datatable/button-ext/jszip.min.js"></script>
<script src="/public/admin-assets/plugins/table/datatable/button-ext/buttons.html5.min.js"></script>
<script src="/public/admin-assets/plugins/table/datatable/button-ext/buttons.print.min.js"></script>
<script>
	$(document).ready(function() {
		$('#html5-extension').DataTable({
			dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
			buttons: {
				buttons: [{
						extend: 'copy',
						className: 'btn'
					},
					{
						extend: 'csv',
						className: 'btn'
					},
					{
						extend: 'excel',
						className: 'btn'
					},
					{
						extend: 'print',
						className: 'btn'
					}
				]
			},
			"oLanguage": {
				"oPaginate": {
					"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
					"sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
				},
				"sInfo": "Showing page _PAGE_ of _PAGES_",
				"sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
				"sSearchPlaceholder": "Search...",
				"sLengthMenu": "Results :  _MENU_",
			},
			"stripeClasses": [],
			"lengthMenu": [7, 10, 20, 50, 100, 500],
			"pageLength": 10,
			drawCallback: function() {
				$('.t-dot').tooltip({
					template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
				})
				$('.dataTables_wrapper table').removeClass('table-striped');
			}
		});
	});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
