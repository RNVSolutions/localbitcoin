<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title> <?= APP_NAME;?> </title>
    <link rel="icon" type="image/x-icon" href="/public/admin-assets/assets/img/favicon.ico"/>
    <link href="/public/admin-assets/assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="/public/admin-assets/assets/js/loader.js"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="/public/admin-assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/admin-assets/assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
	<script src="/public/admin-assets/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="/public/admin-assets/bootstrap/js/popper.min.js"></script>
    <script src="/public/admin-assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/public/admin-assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/public/admin-assets/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="/public/admin-assets/assets/js/custom.js"></script>
	<!-- END GLOBAL MANDATORY SCRIPTS -->
	
</head>
<body>
    <!-- BEGIN LOADER -->
    <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div>
	<!--  END LOADER -->

	
	<?php $this->load->view('template-structure/admin-header-view'); ?>
	<?php $this->load->view('template-structure/admin-sidebar-view'); ?>
	<?php $this->load->view($view);?>
	<?php $this->load->view('template-structure/admin-footer-view'); ?>



	
	
</body>
</html>
