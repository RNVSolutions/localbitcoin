<?php
class LoginModel extends CI_Model
{
    /************************************ Login query ************************************/
    public function user_detail_by_username_or_email($name) {
    	$this->db->select('*');
        $this->db->from('admin');
		$this->db->or_where('username',$name);
		$this->db->or_where('email',$name);
        $query = $this->db->get();
        return $query->row_array();
	}
	public function update_login($userId) {
    	$this->db->set('last_login', date("Y-m-d H:i:s"));
		$this->db->where('id', $userId);
		$this->db->update('admin');
		return true;
	}
	public function update_logout($userId) {
    	$this->db->set('last_logout', date("Y-m-d H:i:s"));
		$this->db->where('id', $userId);
		$this->db->update('admin');
		return true;
	}
	
 } 
 ?>
